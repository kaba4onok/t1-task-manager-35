package ru.t1.rleonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ITokenService getTokenService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

}
