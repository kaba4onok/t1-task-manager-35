package ru.t1.rleonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.model.AbstractUserOwnedModel;
import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    int getSize(@NotNull String userId);

    @Nullable
    M add(@NotNull String userId,
          @NotNull M model);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(
            @NotNull String userId,
            @NotNull Comparator comparator);

    boolean existsById(
            @NotNull String userId,
            @NotNull String id);

    @Nullable
    M findOneById(
            @NotNull String userId,
            @NotNull String id);

    @Nullable
    M findOneByIndex(
            @NotNull String userId,
            @NotNull Integer index);

    @Nullable
    M remove(@NotNull String userId,
             @NotNull M model);

    @Nullable
    M removeById(
            @NotNull String userId,
            @NotNull String id);

    @Nullable
    M removeByIndex(
            @NotNull String userId,
            @NotNull Integer index);

    void clear(@NotNull String userId);

}
