package ru.t1.rleonov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project>, IUserOwnedRepository<Project> {

    @NotNull
    Project create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description);

    @NotNull
    Project create(
            @NotNull String userId,
            @NotNull String name);

}
