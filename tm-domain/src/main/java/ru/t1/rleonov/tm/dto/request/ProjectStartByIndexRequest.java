package ru.t1.rleonov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectStartByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private final Status status = Status.IN_PROGRESS;

    public ProjectStartByIndexRequest(@Nullable String token) {
        super(token);
    }

}
